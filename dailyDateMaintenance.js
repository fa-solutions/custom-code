(async function(lead, context){
  let myappId = lead.instanceId;
  const faEntityId = context.appConfiguration.faEntityId;
  const today = new Date();
  const agentName = context.agent.fullName;


  JSON.safeStringify = (obj, indent = 2) => {
    let cache = [];
    const retVal = JSON.stringify(
      obj,
      (key, value) =>
      typeof value === "object" && value !== null
      ? cache.includes(value)
      ? undefined // Duplicate reference found, discard key
      : cache.push(value) && value // Store value in our collection
      : value,
      indent
    );
    cache = null;
    return retVal;
  };

  let createNote = async (note) => {
    let newNote = {   
      "assignedTo": [],
      "closed_at": null,
      "contacts": [],
      "description": `<div>${note}</div>`,
      "dueDate": today,
      "due_option": null,
      "logo_name": null,
      "note": `<div>${note}</div>`,
      "parent_entity_id": faEntityId,
      "parent_reference_id": myappId,
      "source_name": agentName,
      "status": "closed",
      "task_mentions": [],
      "type": "Note"
    }
    await context.freeagent.addTask(newNote)
  };

  let fieldsToUpdate = [
    {app: 'lead', fields: ["lead_field24", "lead_field22"]},
    {app: 'deal', fields: ["deal_field30", "deal_field19"]},
    {app: 'quote', fields: ["quote_field19", "quote_field20", "quote_field29"]},
    {app: 'approval', fields: ["approval_field1"]}
  ];

  for (var updateConfig of fieldsToUpdate) {
    let {app, fields} = updateConfig;
    let entityValues = await context.freeagent.listEntityValues({entity:app, fields});
    if (entityValues && entityValues.entity_values && entityValues.entity_values.length > 0) {
      for (var i = 0; i < entityValues.entity_values.length; i++) {
        let fieldValues = entityValues.entity_values[i].field_values;
        let updatePackageOpt = {
          "entity": app,
          "id":"",
          "field_values": {}
        };
        for (var field of fields) {
          let currentDate = fieldValues[field] && fieldValues[field].value ? fieldValues[field].value: null;
          if (currentDate) {
            const date = new Date(currentDate);
            date.setDate(date.getDate() + 1);
            updatePackageOpt.field_values[field] = date;
            updatePackageOpt.id = entityValues.entity_values[0].id;
          }
        }
        context.freeagent.updateEntity(updatePackageOpt);
      }
    }
  }

  return 0;
}(lead, context));
