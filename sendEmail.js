
(async function(accountReg, context){
  let url = `https://freeagent.network/account_reg/view/${accountReg.instanceId}`;
  let seqId = accountReg.id;
  
  let response = await context.freeagent.listEntityValues({
     entity: "agent",
     id: accountReg.owner.id
   });
  
  let email = '';
  if(response && response.count > 0) {
     email = response.entity_values[0].field_values.email_address.value;
  }
  
  context.freeagent.sendEmail({
    account: "c3dc2184-0a5d-48ac-a741-bce0699eebda",
    associatedTaskId: null,
    attachtmentsList: [],
    bcc: [],
    bulkContactIds: [],
    cc: [],
    contactId: null,
    isBulk: false,
    msg: `<div>Hi there, Your <a href=\"${url}\" rel=\"noopener noreferrer\" target=\"_blank\">account reg</a> ${accountReg.name} has been rejected. If any question contact Scott Kelly.</div>`,
    no_tracking: true,
    subject: `Freeagent account reg ${seqId} has been rejected`,
    taskId: "",
    templateId: null,
    to: [[accountReg.owner.value, email]]
  })
  
  
  return 0;
}(accountReg, context));


/*

let variables ={
    account: "9ece6602-7d89-48bb-bbed-441f0ad4669d",
    associatedTaskId: null,
    attachtmentsList: [],
    bcc: [], 
    bulkContactIds: ["da0b2813-72ef-4217-a520-d849f8c69f54"],
    cc: [["bob@gmail.com", "bob@gmail.com", "bob@gmail.com"]],
    contactId: "da0b2813-72ef-4217-a520-d849f8c69f54",
    isBulk: false,
    msg: "<div>Test&nbsp;<a href=\"http://google.com\" rel=\"noopener noreferrer\" target=\"_blank\">link</a></div>",
    no_tracking: false,
    subject: "Test",
    taskId: "",
    templateId: null,
    to: [["Bob Mihajlovic", "slobodan.mihajlovic@freeagentsoftware.com"]]
}


let cmcVars = {
    account: "c3dc2184-0a5d-48ac-a741-bce0699eebda",
    associatedTaskId: null,
    attachtmentsList: [],
    bcc: [],
    bulkContactIds: [],
    cc: [],
    contactId: null,
    isBulk: false,
    msg: "<div><a href=\"https://freeagent.network/account_reg/view/{{id}}\" rel=\"noopener noreferrer\" target=\"_blank\">Account request</a> {{seq_id}} has been rejected.</div>",
    no_tracking: true,
    subject: "Freeagent account request has been rejected",
    taskId: "",
    templateId: null,
    to: [["Solutions Team", "solutions@freeagentsoftware.com"]]
}

*/
