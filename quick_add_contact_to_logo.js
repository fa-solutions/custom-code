(function(input, contact, parent, context){
  let inputObject = { logo_id: parent.record_id, first_name: '', last_name: '' };
  if(input && input.length > 0) {
       input.split(',').map(i => {
            if (i && i.includes(':')) {
              i = i.replace(/(^\s+|\s+$)/g,'');
              let key = ''
              if (i.split(':')[0]) {
                  key = i.split(':')[0].replace(/(^\s+|\s+$)/g,'').toLowerCase();
              }
              let value = i.split(':')[1].replace(/(^\s+|\s+$)/g,'');
                if (key === 'n' || key === 'name') {
                   inputObject.first_name = value.split(' ')[0];
                   inputObject.last_name = value.split(' ').slice(1).join(' ');
                }
                if (key === 'fn' || key === 'firstn' || key === 'firstname') {
                   inputObject.first_name = value;
                }
                if (key === 'ln' || key === 'lastn' || key === 'lastname') {
                   inputObject.last_name = value;
                }
                if (key === 'e' || key === 'email') {
                   inputObject.work_email = value;
                }
                if (key === 'p' || key === 'phone') {
                   inputObject.work_phone = value;
                }
                if (key === 't' || key === 'title') {
                   inputObject.current_position = value;
                }
                if (key === 'l' || key === 'loc' || key === 'location') {
                   inputObject.location_name = value;
                }
                if (key === 'li' || key === 'lin' || key === 'linkedin') {
                   inputObject.linkedIn = value;
                }
                if (key === 'w' || key === 'web' || key === 'website' || key === 'site') {
                   inputObject.contact_field15 = value;
                }
            }
      })
  }

  return inputObject;
}(input, contact, parent, context));
