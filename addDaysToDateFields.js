(async function(lead, context){
  // 0 22 * * 1,2,3,4     cron expresion every day from Mon to Thu at 10pm
  let numberOfDays = 1;
  
  let fieldsToUpdate = [
    {app: 'lead', fields: ["lead_field24", "lead_field22"]},
    {app: 'quote', fields: ["quote_field19", "quote_field20", "quote_field29"]},
    {app: 'approval', fields: ["approval_field1"]}
  ];

  let errors = [];
  for (var updateConfig of fieldsToUpdate) {
    let {app, fields} = updateConfig;
    let entityValues = await context.freeagent.listEntityValues({entity:app, fields});
    if (entityValues && entityValues.entity_values && entityValues.entity_values.length > 0) {
      for (var i = 0; i < entityValues.entity_values.length; i++) {
        let fieldValues = entityValues.entity_values[i].field_values;
        let updatePackageOpt = {
          "entity": app,
          "id":"",
          "field_values": {}
        };
        for (var field of fields) {
          let currentDate = fieldValues[field] && fieldValues[field].value ? fieldValues[field].value: null;
          if (currentDate) {
            const date = new Date(currentDate);
            date.setDate(date.getDate() + numberOfDays);
            updatePackageOpt.field_values[field] = date;
            updatePackageOpt.id = entityValues.entity_values[i].id;
          }
        }
        
        try {
          await context.freeagent.updateEntity(updatePackageOpt);
        } catch(e) {
           errors.push(e.message);
        }
      }
    }
  }

  return 0;
}(lead, context));
