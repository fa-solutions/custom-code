(async function(myApp, context){
  const myappId = myApp.instanceId;
  const faEntityId = context.appConfiguration.faEntityId;
  const today = new Date();
  const agentName = context.agent.fullName;

  let createNote = async (note) => {
        let newNote = {   
            "assignedTo": [],
            "closed_at": null,
            "contacts": [],
            "description": `<div>${note}</div>`,
            "dueDate": today,
            "due_option": null,
            "logo_name": null,
            "note": `<div>${note}</div>`,
            "parent_entity_id": faEntityId,
            "parent_reference_id": myappId,
            "source_name": agentName,
            "status": "closed",
            "task_mentions": [],
            "type": "Note"
        }
        await context.freeagent.addTask(newNote)
   };
  
  await createNote('Quote has been sent succesfully')
  
}(myApp, context));