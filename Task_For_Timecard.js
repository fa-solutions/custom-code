(function(timeCard, context){
  const oneDay = 24 * 60 * 60 * 1000;
  const startDate = new Date(timeCard.startDate);
  const endDate = new Date(timeCard.endDate);
  let currentDate = new Date(timeCard.startDate);
  const diffDays = Math.round(Math.abs((startDate - endDate) / oneDay));
  
  for(let i = 0; i < diffDays + 1; i++) {
    if(currentDate.getDay() !== 0 && currentDate.getDay() !== 6) {
      currentDate.setDate(currentDate.getDate() + 1);
      context.freeagent.createEntity({
          entity: "task",
          field_values: {
            task_type: "38f30299-efd0-401d-9cbb-3436c47fc1bb",
            description: timeCard.assignedTo.value,
            task_status: false,
            fa_entity_id: context.appConfiguration.faEntityId,
            fa_entity_reference_id: timeCard.instanceId,
            due_date: currentDate.toISOString(),
            task_field0: timeCard.assignedTo.id,
            task_field1: "#FF5722"
          } 
       })
    } else {
      currentDate.setDate(currentDate.getDate() + 1);
    }
  }
  return 0;
}(timeCard, context));
