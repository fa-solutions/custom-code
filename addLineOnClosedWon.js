(async function(opportunity, context){
    const oppId = opportunity.instanceId;
    const entityId = context.appConfiguration.faEntityId;
    const agentName = context.agent.fullName;
    const today = new Date();
    const ownerId = opportunity.owner.id;
  
  
  JSON.safeStringify  = (obj, indent = 2) => {
  let cache = [];
  const retVal = JSON.stringify(
    obj,
    (key, value) =>
      typeof value === "object" && value !== null
        ? cache.includes(value)
          ? undefined // Duplicate reference found, discard key
          : cache.push(value) && value // Store value in our collection
        : value,
    indent
  );
  cache = null;
  return retVal;
};
  
  let createNote = async (note) => {
        let newNote = {   
            "assignedTo": [],
            "closed_at": null,
            "contacts": [],
            "description": `<div>${note}</div>`,
            "dueDate": today,
            "due_option": null,
            "logo_name": null,
            "note": `<div>${note}</div>`,
            "parent_entity_id": entityId,
            "parent_reference_id": oppId,
            "source_name": agentName,
            "status": "closed",
            "task_mentions": [],
            "type": "Note"
        }
        await context.freeagent.addTask(newNote)
   };
  
  
    const getEntityDoc = {
        "entity" : "personnel",
        "filters": [{field_name: "owner_id", values: [ownerId]}],
        "fields_from_card_layout" : false,
        "order" : [["created_at", "DESC"]],
        "offset" : 0
    };
    let salesGolaId = null;
    try{
        let lastDocument = await context.freeagent.listEntityValues(getEntityDoc);
        if(lastDocument && lastDocument.entity_values && lastDocument.entity_values.length > 0){
            lastDocument.entity_values.map(record => {
              let endDateField = record &&record.field_values && record.field_values.personnel_field6;
              let startDate = record &&record.field_values && record.field_values.personnel_field5;
              if (!salesGolaId && endDateField && startDate) {
                let closedDateMili = new Date(opportunity.closeDate).getTime();
                let startDateMili = new Date(record.field_values.personnel_field5.value).getTime();
                let endDateMili = new Date(record.field_values.personnel_field6.value).getTime();
                if(closedDateMili < endDateMili && closedDateMili > startDateMili) {
                  salesGolaId = record.id;
                }
              }
            });
        } 
        if (salesGolaId) {
           let addLinesOptions = {
             "entity": "personnel",
             "id": salesGolaId,
             "field_values": {},
             "children": [{"entity": "sale", "field_values": {"sale_field0": oppId}}]
           }
           try {
             await context.freeagent.upsertCompositeEntity(addLinesOptions);
             return null;
           } catch(e) {
               return {success:false, message: 'Failed to add the line'};
           }
        } else {
          return {success:false};
        }
    } catch(e) {
        return {success:false};
    }
}(opportunity, context));