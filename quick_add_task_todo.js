(function(input, task, parent, context){
  
var dueDateText = null;
var description = input;
if (input && input.includes('day')) {
    var numOfDays = input.replace(/[^0-9.]/g, '');
    var date = new Date()

  date.setDate(date.getDate() + parseFloat(numOfDays))
  dueDateText = date.toString();
  description = description.replace(`in ${numOfDays} days`,"")
  description = description.replace(`in ${numOfDays} day`,"")
  description = description.replace(`${numOfDays} days`,"")
  description = description.replace(`${numOfDays} day`,"")
}
   
  // task_type: task.taskType.meeting
  // event_start_time: 'November 25, 2020 11:00 AM',
  // event_end_time: 'November 25, 2020 11:15 AM',

    return {
      description: description,
      due_date: dueDateText,
      task_type: task.taskType.toDo,
      fa_entity_id: parent.app_id,
      fa_entity_reference_id: parent.record_id,
      assigned_to: context.profile.id,
    };
  
  
  }(input, task, parent, context));