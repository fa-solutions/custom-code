(async function(task, context){
  if (task.startTime && task.startTime !== '' && task.type.value === 'Meeting') {
    const options = {
          method: 'POST',
          data: task,
          url: 'https://hooks.zapier.com/hooks/catch/******/*******'
       };
  await context.libs.axios(options)
    return 0;
  } else {
    return {success:true, message: 'Task type must be meeting and needs to include start date'}
  }
  
}(task, context));