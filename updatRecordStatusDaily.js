(async function(quote, context){
    let expiredQuotes = await context.freeagent.listEntityValues({
        "entity": "quote",
        "filters": [
            {
                "field_name": "quote_field16",
                "operator": "period",
                "values": [
                    "Today"
                ]
            },
            {
                "field_name": "quote_field1",
                "operator": "excludes",
                "values": [
                    "2202d868-ffa4-4d37-8caa-dd79be43159b"
                ]
            }
        ]
    })
    
    if(expiredQuotes && expiredQuotes.entity_values && expiredQuotes.count > 0) {
        for (let i = 0; i < expiredQuotes.count; i++) {
        let quote = expiredQuotes.entity_values[i];
        await context.freeagent.upsertCompositeEntity({
                children: [],
                entity: "quote",
                id: quote.id,
                m2m_custom_field: null,
                m2m_instance_id: null,
                field_values: {
                quote_field1: "2202d868-ffa4-4d37-8caa-dd79be43159b"
                }
            })
        }
    }
    return 0;
}(quote, context));
