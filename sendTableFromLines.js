(async function(testApp, context){
  let response = await context.freeagent.listEntityValues({
     entity: "agent",
     id: testApp.owner.id
   });
  
  let email = '';
  if(response && response.count > 0) {
     email = response.entity_values[0].field_values.email_address.value;
  }
  
  let tableHtml = `<div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><table border="0" cellpadding="0" width="850" style="width:637.5pt"><tbody><tr><th width="14%" style="width:115.640625px;padding:0in"><p class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Item #</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="20%" style="width:165.203125px;padding:0in"><p class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Description</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Quantity</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="6%" style="width:49.5625px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Unit.$</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Ext.$</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Comm.%</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Alt.Item?</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="5%" style="width:41.3125px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">LC</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="right" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Available</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="10%" style="width:82.609375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Competitor</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th></tr>{{tableRows}}</tbody></table></div>`
 
  let tableRows = [];
  
testApp.testLines.map(line => {
  let item = line.item;
  let description = line.itemDescription;
  let qty = line.quantity;
  let price = line.unit;
  let commission = line.comm;
  let lc = 'LC4';
  let competitor = 'COMPETITOR';
  let lineTotal = '167,475.00';
  let available = 'Available';
 
 
  let lineHtml = `<tr><td width="14%" style="width:115.640625px;padding:0in"><p class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${item}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="22%" style="width:170.203125px;padding:0in"><p class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${description}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="8%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${qty}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="6%" style="width:48.5625px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${price}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="8%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="right" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:right"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${lineTotal}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="8%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${commission}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="10%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"></td><td width="5%" style="width:40.3125px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${lc}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="8%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="right" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:right"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${available}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="10%" style="width:81.609375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${competitor}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td></tr>`
  
  tableRows.push(lineHtml)
})
  
  
  let jobName = 'HARNETT COUNNTY';
  let probablity = '50 %';
  let customer = '11SHE0 (SHEPHERD ELECTRIC SUPPLY)';
  let createdBy = 'Rick Allis';
  let salesman = 'Eric Schlechtweg';
  let quoteTotal = '219,875.00';
  let quoteDate = '06/04/2021';
  let targetDate = '06/24/2021';
  
  let headerTable = `<table border="0" cellpadding="0" width="850" style="width:637.5pt"><tbody><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">* Please note that we received quote over 100K</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Job Name: ${jobName}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Quote Date: ${quoteDate}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Target Date: ${targetDate}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Probability: ${probablity}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Customer: ${customer}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Salesman #1: ${salesman}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Quoted By: ${createdBy}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif">Quote Update:</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr><tr><td style="padding:2.25pt"><div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif;color:red">Quote $: ${quoteTotal}</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></div></td></tr></tbody></table>`;
  
  let tableHtmlWithLines = tableHtml.replace('{{tableRows}}', tableRows.join())
 
  let quoteDetails = `<div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><table border="0" cellpadding="0" width="850" style="width:637.5pt"><tbody><tr><td style="background-color:green;padding:2.25pt;background-position:initial initial;background-repeat:initial initial"><p class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-family:Verdana,sans-serif;color:white">Quote Detail</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></td></tr></tbody></table></div>`;
  
   const myappId = testApp.instanceId;
  const faEntityId = context.appConfiguration.faEntityId;
  const today = new Date();
  const agentName = context.agent.fullName;

  let createNote = async (note) => {
        let newNote = {   
            "assignedTo": [],
            "closed_at": null,
            "contacts": [],
            "description": note,
            "dueDate": today,
            "due_option": null,
            "logo_name": null,
            "note": note,
            "parent_entity_id": faEntityId,
            "parent_reference_id": myappId,
            "source_name": agentName,
            "status": "closed",
            "task_mentions": [],
            "type": "Note"
        }
        await context.freeagent.addTask(newNote)
   };
  
  await createNote(`<div>${headerTable}${quoteDetails}${tableHtmlWithLines}</div>`)
 
  context.freeagent.sendEmail({
    account: "9ece6602-7d89-48bb-bbed-441f0ad4669d",
    associatedTaskId: null,
    attachtmentsList: [],
    bcc: [],
    bulkContactIds: [],
    cc: [],
    contactId: null,
    isBulk: false,
    msg: `<div>${headerTable}${quoteDetails}${tableHtmlWithLines}</div>`,
    no_tracking: true,
    subject: `Quote over 100k has been created`,
    taskId: "",
    templateId: null,
    to: [['Bob', 'bobanbrban@gmail.com']]
  })
  
  return 0;

}(testApp, context));
