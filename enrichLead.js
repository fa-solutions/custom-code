(async function(lead, context){
    const oppId = lead.instanceId;
    const entityId = context.appConfiguration.faEntityId;
    const agentName = context.agent.fullName;
    const today = new Date();
   
  JSON.safeStringify  = (obj, indent = 2) => {
  let cache = [];
  const retVal = JSON.stringify(
    obj,
    (key, value) =>
      typeof value === "object" && value !== null
        ? cache.includes(value)
          ? undefined // Duplicate reference found, discard key
          : cache.push(value) && value // Store value in our collection
        : value,
    indent
  );
  cache = null;
  return retVal;
};
  
  let createNote = async (note) => {
        let newNote = {   
            "assignedTo": [],
            "closed_at": null,
            "contacts": [],
            "description": `<div>${note}</div>`,
            "dueDate": today,
            "due_option": null,
            "logo_name": null,
            "note": `<div>${note}</div>`,
            "parent_entity_id": entityId,
            "parent_reference_id": oppId,
            "source_name": agentName,
            "status": "closed",
            "task_mentions": [],
            "type": "Note"
        }
        await context.freeagent.addTask(newNote)
   };

  
  const oAuthOptions = {
        method: 'GET',
        headers: {'Authorization': 'Bearer <<api_key>>'},
        url: `https://person-stream.clearbit.com/v2/combined/find?email=${lead.email}`
    };
    let res = await context.libs.axios(oAuthOptions);
   await createNote(JSON.safeStringify(res.data));
  return 0;
}(lead, context));
