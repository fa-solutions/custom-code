(async function(lead, context){
    const leadId = lead.instanceId;
    const today = new Date();
    const dealerId = lead.dealerAssigned.id;

  if (dealerId) {
    if(lead.stage.id !== '9d145a8e-1920-4320-a3b7-9f34ad2faf2a') {
      let addLinesOptions = {
        "entity": "dealer",
        "id": dealerId,
        "field_values": {},
        "children": [{"entity": "lead_assigned", "field_values": {"lead_assigned_field0": leadId, "lead_assigned_field2": today}}]
      }
      try {
        await context.freeagent.upsertCompositeEntity(addLinesOptions);
        let updatePackageOpt = {
               "entity":"lead",
               "id":"id",
               "field_values": {
                  "lead_field11":"9d145a8e-1920-4320-a3b7-9f34ad2faf2a"
               }
         };
        updatePackageOpt.id = leadId;
        await context.freeagent.updateEntity(updatePackageOpt);
        return null;
      } catch(e) {
        return {success:false, message: 'Failed to add the line'};
      }
    } else {
      return {success:false, message: "Dealer has been assigned already"};
    }
  } else {
    return {success:false, message: "No Dealer Assigned"};
  }
}(lead, context));