(async function(quote, context){
    let hasLc = false;
    let hasLowThanHot = false;

    let allPromisses = [];

    const myappId = quote.instanceId;
    const faEntityId = context.appConfiguration.faEntityId;
    const today = new Date();
    const agentName = context.agent.fullName;

    let createNote = async (note) => {
        let newNote = {
            assignedTo: [],
            closed_at: null,
            contacts: [],
            description: note,
            dueDate: today,
            due_option: null,
            logo_name: null,
            note: note,
            parent_entity_id: faEntityId,
            parent_reference_id: myappId,
            source_name: agentName,
            status: 'closed',
            task_mentions: [],
            type: 'Note',
        };
        await context.freeagent.addTask(newNote);
    };
    

   let tableHtml = `<div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><table style="border:border-collapse" border="0" cellpadding="0" width="850" style="width:637.5pt"><tbody><tr style="padding:10px;background:#86c4fc;border: 1px solid #ddd"><th width="14%" style="width:115.640625px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Item #</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="20%" style="width:165.203125px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Reason</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Quantity</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="6%" style="width:49.5625px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Unit Price</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th><th width="8%" style="width:66.09375px;padding:0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><b><span style="font-size:7.5pt;font-family:Verdana,sans-serif">Line Amount</span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></th></tr>{{tableRows}}</tbody></table></div>`;

    let tableRows = [];

    quote.quoteItems.map(line => {
        let item = line.itemFormatted ? line.itemFormatted : '';
        let qty = line.quantityFormatted;
        let price = line.priceFormatted;
        let hotPrice = line.hotPrice;
        let lineTotal = line.lineAmountFormatted;
        let lc = line.lifeCycleFormatted
        let isLC2 = lc === 'LC2';
        let isLowThanHot = (1 - (parseFloat(line.price || 0) / parseFloat(hotPrice || 0)) > 0.05);
        let reason = isLowThanHot ? '5% Lower than Hot' : 'LC2 Item';


        let lineHtml = `<tr><td width="14%" style="width:115.640625px;padding:0in"><p align="center" class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${item}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="22%" style="width:170.203125px;padding:0in"><p align="center" class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${reason}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="8%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${qty}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="6%" style="width:48.5625px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${price}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td><td width="8%" style="width:65.09375px;padding:0in 0.75pt 0in 0in"><p class="MsoNormal" align="center" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif;text-align:center"><span style="font-size:7.5pt;font-family:Verdana,sans-serif">${lineTotal}</span><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></p></td></tr>`

        if (isLC2 || isLowThanHot) {
            tableRows.push(lineHtml);
            if(isLC2) {
                hasLc = true;
            }
            if(isLowThanHot) {
                hasLowThanHot = true;
            }
        }
    })
     
    if(tableRows.length === 0) {
        return 0;
    }

    let tableHtmlWithLines = tableHtml.replace(
        '{{tableRows}}',
        tableRows.join()
    );

    allPromisses.push(createNote(`<div><h2>Approval Required!</h1><hr> <h2>Quote lines: </h3>${tableHtmlWithLines}</div>`));

    let url = `https://freeagent.network/quote/view/${quote.instanceId}`;
    let quoteDetails = `<div style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><table border="0" cellpadding="0" width="850" style="width:637.5pt"><tbody><tr><td style="background-color:green;padding:2.25pt;background-position:initial initial;background-repeat:initial initial"><p class="MsoNormal" style="margin:0in;font-size:10pt;font-family:Calibri,sans-serif"><b><span><a style="font-family:Verdana,sans-serif;color:white" href="${url}" target="_blank">Quote Detail</a></span></b><b><span style="font-size:11pt;font-family:Verdana,sans-serif"><u></u><u></u></span></b></p></td></tr></tbody></table></div>`;

    let toEmail = [['rallis@maxlite.com', 'rallis@maxlite.com']]
    if(context.agent.fullName === "Matthew Arcila") {
       toEmail = [['marcila@maxlite.com', 'marcila@maxlite.com']]
        // toEmail = [['slobodan.mihajlovic@freeagentsoftware.com', 'slobodan.mihajlovic@freeagentsoftware.com']]
    }


   allPromisses.push(context.freeagent.sendEmail({
        attachtmentsList: [],
        account: '2965fa0f-4da4-46f8-86b2-dc993a21035e',
        contactId: null,
        bcc: [],
        bulkContactIds: [],
        cc: [],
        isBulk: false,
        msg: `<div><h1>Approval Required!</h1><hr> <h3>Quote lines: ${quoteDetails}</h3>${tableHtmlWithLines}</div>`,
        no_tracking: false,
        subject: `Quote Approval Needed[Quote #: ${quote.quoteNo}, Job Name: ${quote.jobName}]`,
        to: toEmail,
    }));


    let updateQuote = {
        "entity":"quote",
        "id": quote.instanceId,
        "field_values": {}
    };
    let approvalNeededFor = []
    if(hasLowThanHot || hasLc) {
        updateQuote.field_values.quote_field37 = "8dc64bf4-95e1-4b75-b370-dd0144a1312c"
        
        if(hasLowThanHot) {
            approvalNeededFor.push("2ba46249-63c0-4697-8be7-4d2a06e29408");
        }
        if(hasLc) {
            approvalNeededFor.push("b4ab8b83-c875-4b2b-99b9-c19e83489d4f");
        }
        updateQuote.field_values.quote_field48 = approvalNeededFor;
    } else {
        updateQuote.field_values.quote_field37 = "065242d7-9a9b-417f-b49e-000da577b530"
    }
    allPromisses.push(context.freeagent.updateEntity(updateQuote));

    let assignedTo = "e8fab052-4583-4735-ae52-8a9eafa83771"; //Rick Allis
    if(context.agent.fullName === "Matthew Arcila") {
        assignedTo = "e8fab052-4583-4735-ae52-8a9eafa83771";// Matt A
    }

   async function createApprovalTask() {
        let createApprovalPayload = {
              "entity": "task",
              "field_values": {
                  "task_type": "b295d5f5-1972-4a30-b91e-639e29d1b715",
                  "task_status": false,
                  "overdue": false,
                  "send_automatically": false,
                  "send_invitations": false,
                  "assigned_to": assignedTo,
                  "description": `${quote.quoteNo} - Quote Approval Needed`,
                  "fa_entity_id": faEntityId,
                  "fa_entity_reference_id": myappId,
                  "task_field1": approvalNeededFor,
                  "task_field3": myappId,
                  "task_field12": false,
                  "all_day": false
              }
          }
        
        
        let taskCreated = await context.freeagent.createEntity(createApprovalPayload);
       
     
        let tutorialNote = {
            "assignedTo": [],
            "closed_at": null,
            "contacts": [],
            "description": `<h1>Approval Needed</h1><hr>${tableHtmlWithLines}`,
            "dueDate": today,
            "due_option": null,
            "logo_name": null,
            "note":`<h1>Approval Needed</h1><hr>${tableHtmlWithLines}`,
            "parent_entity_id": faEntityId,
            "parent_reference_id": myappId,
            "source_name": agentName,
            "status": "closed",
            "task_mentions": [],
            "type": "Note"
        }

        if(taskCreated && taskCreated.entity_value){
            tutorialNote.parent_entity_id = taskCreated.entity_value.fa_entity.id;
            tutorialNote.parent_reference_id = taskCreated.entity_value.id;
            return await context.freeagent.addTask(tutorialNote)
        }

				return null;
    }

    allPromisses.push(createApprovalTask());

    return await Promise.all(allPromisses).catch(e => e);
}(quote, context));
