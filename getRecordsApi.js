 var clientId = "";
 var clientSecret = "";
  
  async function getToken(clientId, clientSecret) {
    const oAuthOptions = {
        method: 'POST',
        headers: {'content-type': 'application/json'},
        data: `{"grant_type":"client_credentials","client_id": "${clientId}","client_secret": "${clientSecret}"}`,
        url: 'https://freeagent.network/oauth/token'
    };
    return context.libs.axios(oAuthOptions)
}
  
   let res = await getToken(clientId, clientSecret);
   let token = res.data.access_token;
  
 
async function fetchQuery(queryStr) {
    let axios = context.libs.axios.create({headers: { 'Content-Type': 'application/json', "Authorization" : "Bearer " + token}, baseURL:  'https://freeagent.network/api/graphql', timeout: 1000000});
  
   const queryOptions = {
     method: 'POST',
     data: JSON.stringify({query: queryStr}),
    };
    try {
        let queryRes = await axios(queryOptions);
        return queryRes.data.data
    } catch (e) {
        return e;
    }
}
  
  
  let getRecords = async (options) => {
        if (options) {
            let listEntityValues = await context.freeagent.listEntityValues(options);
            if (listEntityValues && listEntityValues.entity_values && listEntityValues.entity_values.length > 0) {
                return listEntityValues.entity_values;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
  
  
const getEntities = async () => {
        const graphqlQuery =  `query { getEntities(show_hidden: false, only_referenceable: true, include_non_primary: false) { id name is_custom parent_id entity_id  } }`;
        var res = await fetchQuery(graphqlQuery);
        return res.getEntities;
}


const getFieldItems = async (id) => {
    var fieldItems = await context.freeagent.getFieldItems({fa_field_config_id: id});
    if (fieldItems && fieldItems.getFieldItems) {
        fieldItems = fieldItems.getFieldItems;
    }
    fieldItems = fieldItems.map(field => {
        return { value: field.id, label: field.name };
    });
    return fieldItems;
}

var getFieldsBodyMin = `id is_custom main_type is_calculated name entity name_label is_required render_type reference_fa_entity_id reference_field_id fa_field_id custom_field_id related_list_name related_list_name_plural is_display_field calculated_function
    is_calculated`;
  
const findFieldsByEntity = async (entityId) => {
    if (entityId) {
          let query = `{ getFields(entity: "${entityId}") { ${getFieldsBodyMin} } }`;
          let fields = await fetchQuery(query)

            if (fields && fields.getFields) {
                fields = fields.getFields;
            }
            return fields;
     } else {
      return [];
     }
}

let getEntityRecordsByEntity = async (entityName) => {
     let options = {
        "entity" : entityName,
        "fields_from_card_layout" : false,
        "order" : [["created_at", "DESC"]],
        "offset" : 0
     };
  let listEntityValues = await context.freeagent.listEntityValues(options);
  return listEntityValues.entity_values;
}
